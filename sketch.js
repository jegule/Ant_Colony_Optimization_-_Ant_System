var ALPHA = 1;

var BETA = 2;

//Pheromone Evaportaion Coefficient
var EVAPORATION_COEFFICIENT = 0.5;

//Number of iteration
var MAX_ITERATION = 500;

//Number of Ants
var NUMBER_OF_ANTS = 10;

//Total Number of City
var NUMBER_OF_CITIES = 5;

//Starts from 0
var DEPARTURE_CITY = 0;

//Distance City Matrix
var DISTANCE_MATRIX = [
  [0, 10, 12, 11, 14],
  [10, 0, 13, 15, 8],
  [12, 13, 0, 9, 14],
  [11, 15, 9, 0 ,16],
  [14, 8, 14, 16, 0]
];

var PHEROMONE_MATRIX = [];

var minimumDistance = -1;

var maximumDistance = -1;

var bestRoute = [];

var worstRoute = [];

function setup(){

  var VISIBILITY_MATRIX = [];

  for (var i = 0; i < DISTANCE_MATRIX.length; i++){
    // Initialize temporary matrix
    var tempDistanceMatrix = [];
    var tempPheromoneMatrix = [];

    for (var j = 0; j < DISTANCE_MATRIX[i].length; j++){
      // Get current distance (d)
      var currentDistance = DISTANCE_MATRIX[i][j];

      /*
        If current distance == 0, just add 0 to matrix
        If current distance != 0, add 1/d to matrix
      */
      if(currentDistance == 0) tempDistanceMatrix.push(0);
      else tempDistanceMatrix.push(1/currentDistance);

      tempPheromoneMatrix.push(1);

    }

    //Push to real matrix
    VISIBILITY_MATRIX.push(tempDistanceMatrix);
    PHEROMONE_MATRIX.push(tempPheromoneMatrix);

  }

  // var INITIAL_VISIBILITY_MATRIX = VISIBILITY_MATRIX.clone();
  var INITIAL_VISIBILITY_MATRIX = [];
  angular.copy(VISIBILITY_MATRIX, INITIAL_VISIBILITY_MATRIX);

  for (var i = 0; i < INITIAL_VISIBILITY_MATRIX.length; i++){
    INITIAL_VISIBILITY_MATRIX[i][DEPARTURE_CITY] = 0;
  }

  var currentIteration = 0;

  while (currentIteration < MAX_ITERATION) {

    var antsVisibilityMatrix = [];

    //Loop through each ants
    for (var i = 0; i < NUMBER_OF_ANTS; i++){
      // var tempInitialVisibilityMatrix = INITIAL_VISIBILITY_MATRIX.clone();
      var tempInitialVisibilityMatrix = [];
      angular.copy(INITIAL_VISIBILITY_MATRIX, tempInitialVisibilityMatrix);

      antsVisibilityMatrix.push(tempInitialVisibilityMatrix);
      // printArray(antsVisibilityMatrix[i]);
    }

    console.log("Iteration: " + (currentIteration+1));

    var distanceMatrix = [];
    var travelledCityMatrix = [];

    for (var i = 0; i < NUMBER_OF_ANTS; i++){

      var currentCity = DEPARTURE_CITY;

      var travelledCity = [currentCity];

      // console.log("Current ant: " + (i+1));
      // console.log("Travelled City: ");
      // console.log(travelledCity);

      while(travelledCity.length < NUMBER_OF_CITIES){

        var tempWeightMatrix = [];
        var tempProbabilityMatrix = [];
        var tempCumulativeProbabilityMatrix = [];
        var totalTempWeight = 0;

        for (var j = 0; j < NUMBER_OF_CITIES; j++){

          // printArray(antsVisibilityMatrix[i]);

          var currentVisiblityToDestCity = antsVisibilityMatrix[i][currentCity][j];
          // console.log("Current Visibility To Dest: " + currentVisiblityToDestCity);

          var currentPheromone = PHEROMONE_MATRIX[currentCity][j];
          // console.log("Current Pheromone: " + currentPheromone);

          var weight = Math.pow(currentPheromone, ALPHA) * Math.pow(currentVisiblityToDestCity, BETA)

          tempWeightMatrix.push(weight);

          totalTempWeight += weight;
        }

        // console.log("Weight Matrix: ");
        // console.log(tempWeightMatrix);

        var prevCumulativeProbability = 0;

        for (var j = 0; j < tempWeightMatrix.length; j++){

          var probability = tempWeightMatrix[j]/totalTempWeight;
          tempProbabilityMatrix.push(probability);

          prevCumulativeProbability += probability
          tempCumulativeProbabilityMatrix.push(prevCumulativeProbability);
        }

        // console.log("Cumulative Probability Matrix: ");
        // console.log(tempCumulativeProbabilityMatrix);

        var randomNumber = Math.random();

        var selectedRandomCity = 0;

        for (var j = 0; j < tempCumulativeProbabilityMatrix.length; j++){
          if(tempProbabilityMatrix[j] != 0){
            if(randomNumber < tempCumulativeProbabilityMatrix[j]){
              selectedRandomCity = j;
              break;
            }
          }
        }

        var currentAntVisibilityMatrix = antsVisibilityMatrix[i];

        for (var j = 0; j < currentAntVisibilityMatrix.length; j++){
          currentAntVisibilityMatrix[j][selectedRandomCity] = 0;
        }

        // console.log("City: " + (selectedRandomCity+1));
        // printArray(currentAntVisibilityMatrix);

        travelledCity.push(selectedRandomCity);
        currentCity = selectedRandomCity;

      }

      console.log(travelledCity);
      travelledCity.push(DEPARTURE_CITY);

      var tempTravelledCity = angular.copy(travelledCity);
      travelledCityMatrix.push(tempTravelledCity);

      var totalDistance = calculateDistance(travelledCity);

      if(minimumDistance == -1) {
        minimumDistance = totalDistance;
        bestRoute = angular.copy(travelledCity);
      }
      if(maximumDistance == -1){
        maximumDistance = totalDistance;
        worstRoute = angular.copy(travelledCity);
      }

      if(totalDistance < minimumDistance){
        minimumDistance = totalDistance;
        bestRoute = angular.copy(travelledCity);
      }
      if(totalDistance > maximumDistance){
        maximumDistance = totalDistance;
        worstRoute = angular.copy(travelledCity);
      }

      console.log(totalDistance);
      distanceMatrix.push(totalDistance);
    }

    console.log(distanceMatrix);
    updatePheromonMatrix(distanceMatrix, travelledCityMatrix);

    // console.log(PHEROMONE_MATRIX);

    // printArray(PHEROMONE_MATRIX);

    currentIteration += 1;
  }

  // console.log(antsVisibilityMatrix);

  console.log("Maximum Distance: " + maximumDistance);
  console.log("Worst Route: " + worstRoute);

  console.log("Minimum Distance: " + minimumDistance);
  console.log("Best Route: " + bestRoute);


}

function printArray(theArray){
  for(var i = 0; i < theArray.length; i++){
    console.log(theArray[i]);
  }
}

function calculateDistance(cityArray){
  var sumDist = 0;
  for(var i = 1; i < cityArray.length; i++){
    var prevCity = cityArray[i-1];
    var currCity = cityArray[i];

    sumDist += DISTANCE_MATRIX[prevCity][currCity];
  }

  return sumDist;
}

function updatePheromonMatrix(distanceMatrix, travelledCityMatrix){
  var decayRate = (1 - EVAPORATION_COEFFICIENT);

  var tempCumulativePheromoneDeposit = intializeMatrix(NUMBER_OF_CITIES);

  for(var i = 0; i < travelledCityMatrix.length; i++){
    for(var j = 0; j < NUMBER_OF_CITIES; j++){
      var currentFromPath = travelledCityMatrix[i][j];
      var currentToPath = travelledCityMatrix[i][j+1];

      tempCumulativePheromoneDeposit[currentFromPath][currentToPath] += 1/distanceMatrix[i];
    }
  }

  for(var i = 0; i < PHEROMONE_MATRIX.length; i++){
    for(var j = 0; j < PHEROMONE_MATRIX[i].length; j++){
      PHEROMONE_MATRIX[i][j] = (decayRate*PHEROMONE_MATRIX[i][j]) + tempCumulativePheromoneDeposit[i][j];
    }
  }
}

function intializeMatrix(length){
  var array = [];
  for(var i = 0; i < length; i++){
    var tempArray = [];
    for(var j = 0; j < length; j++){
      tempArray.push(0);
    }
    array.push(tempArray);
  }

  return array;
}
